package mines.projects.graph;

import mines.projects.graph.display.GraphDisplayOptions;
import mines.projects.graph.display.GraphToDot;
import mines.projects.graph.display.WindowsLauncher;
import mines.projects.graph.representation.directed.AdjacencyMatrixDirectedGraph;

public class FloydTest {
    private final static boolean DISPLAY = false;

    public static void main(String[] args) {
        int matrix[][] = new int[][] {
                {0, 1, 0, 1, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 0, 1, 1},
                {0, 1, 0, 0, 1},
                {0, 0, 0, 0, 0}
        };

        AdjacencyMatrixDirectedGraph graph = new AdjacencyMatrixDirectedGraph(matrix);

        // Modification des poids
        graph.setWeight(0, 1, 1);
        graph.setWeight(0, 3, 5);
        graph.setWeight(1, 2, 3);
        graph.setWeight(2, 3, 0);
        graph.setWeight(2, 4, 2);
        graph.setWeight(3, 1, 1);
        graph.setWeight(3, 4, 1);

        if (DISPLAY) {
            GraphDisplayOptions.getGraphDisplayOptions().displayArcCosts = true;
            GraphDisplayOptions.getGraphDisplayOptions().launcher = new WindowsLauncher("C:\\Program Files (x86)\\Graphviz2.38\\bin");
            GraphToDot.displayImage(GraphToDot.graphToDot(false, graph), "GraphePondere");
        }
        
        Object[] result = GraphTools.floyd(graph);
        int[][] distances = (int[][]) result[0];
        int[][] successors = (int[][]) result[1];

        System.out.println(GraphTools.matrixToString(distances));
        final int x = 3;
        final int y = 1;
        System.out.println("Chemin le plus court entre le noeud "+x+" et "+y+" est " + GraphTools.arrayToString(GraphTools.getPathFromFloydSuccessorsMatrix(successors, x, y)) + " avec une distance de " + distances[x][y]);
    }
}
