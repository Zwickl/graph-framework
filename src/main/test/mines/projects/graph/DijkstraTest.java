package mines.projects.graph;

import mines.projects.graph.display.GraphDisplayOptions;
import mines.projects.graph.display.GraphToDot;
import mines.projects.graph.display.WindowsLauncher;
import mines.projects.graph.representation.directed.AdjacencyMatrixDirectedGraph;

public class DijkstraTest {
    private final static boolean DISPLAY = false;

    public static void main(String args[]) {
        int matrix[][] = new int[][]{
                {0, 1, 0, 1, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 0, 1, 1},
                {0, 1, 0, 0, 1},
                {0, 0, 0, 0, 0}
        };

        AdjacencyMatrixDirectedGraph graph = new AdjacencyMatrixDirectedGraph(matrix);

        // Modification des poids
        graph.setWeight(0, 1, 1);
        graph.setWeight(0, 3, 5);
        graph.setWeight(1, 2, 3);
        graph.setWeight(2, 3, 0);
        graph.setWeight(2, 4, 2);
        graph.setWeight(3, 1, 1);
        graph.setWeight(3, 4, 1);

        if (DISPLAY) {
            GraphDisplayOptions.getGraphDisplayOptions().displayArcCosts = true;
            GraphDisplayOptions.getGraphDisplayOptions().launcher = new WindowsLauncher("C:\\Program Files (x86)\\Graphviz2.38\\bin");
            GraphToDot.displayImage(GraphToDot.graphToDot(false, graph), "GraphePondere");
        }

        Object[] data = GraphTools.dijkstra(graph, 0);

        int[] distance = (int[]) data[0];
        int[] pred = (int[]) data[1];

        System.out.println("Distances : " + GraphTools.arrayToString(distance));
        System.out.println("Predecesseurs : " + GraphTools.arrayToString(pred));
    }
}
