package mines.projects.graph.representation.tree;

import mines.projects.graph.Node;
import mines.projects.graph.display.GraphDisplayOptions;
import mines.projects.graph.display.GraphToDot;
import mines.projects.graph.display.WindowsLauncher;

public class BinaryTreeTest {
    private final static boolean DISPLAY = true;

    public static void main(String args[]) {
        BinaryTree tree = new BinaryTree();
        tree.insertValue(new Node(0, 11));
        tree.insertValue(new Node(1, 10));
        tree.insertValue(new Node(2, 6));
        tree.insertValue(new Node(3, 12));
        tree.insertValue(new Node(4, 14));
        tree.insertValue(new Node(5, 9));
        tree.insertValue(new Node(6, 4));

        if (DISPLAY) {
            GraphDisplayOptions.getGraphDisplayOptions().useValue = true;
            GraphDisplayOptions.getGraphDisplayOptions().launcher = new WindowsLauncher("C:\\Program Files (x86)\\Graphviz2.38\\bin");
            GraphToDot.displayImage(GraphToDot.graphToDot(false, tree), "GraphePondere");
        }

        for (Node n : tree.getTree()) {
            System.out.println(n);
        }
        System.out.println("-------------");
        System.out.println("Test remove");
        tree.removeLowestValue();
        for (Node n : tree.getTree()) {
            System.out.println(n);
        }


    }
}
