package mines.projects.graph.representation.directed;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;


/**
 * Test class for adjacency matrix based directed graphs
 *
 * @author Paul Defois
 */
public class AdjacencyMatrixDirectedGraphTest {
    private final int[][] VAL_MATRIX = new int[][]{
            new int[]{0, 0, 0, 1}, // 0
            new int[]{1, 0, 1, 0}, // 1
            new int[]{0, 1, 0, 1}, // 2
            new int[]{0, 1, 1, 0}  // 3
            //        0  1  2  3
    };
    private final int[][] VAL_MATRIX_TRANSPOSE = new int[][]{
            new int[]{0, 1, 0, 0}, // 0
            new int[]{0, 0, 1, 1}, // 1
            new int[]{0, 1, 0, 1}, // 2
            new int[]{1, 0, 1, 0}  // 3
            //        0  1  2  3
    };
    private AdjacencyMatrixDirectedGraph graph;

    @Before
    public void setup() {
        this.graph = new AdjacencyMatrixDirectedGraph(VAL_MATRIX);
    }

    @Test
    public void testGetNbNodes() {
        assertEquals(graph.getNbNodes(), 4);
    }

    @Test
    public void testGetNbEdges() {
        assertEquals(graph.getNbArcs(), 7);
    }

    @Test
    public void testToAdjacencyMatrix() {
        assertEquals(graph.toAdjacencyMatrix(), VAL_MATRIX);
    }

    @Test
    public void testGetAdjacencyMatrix() {
        assertEquals(graph.getAdjacencyMatrix(), VAL_MATRIX);
    }

    @Test
    public void testIsArc() {
        assertTrue(graph.isArc(1, 0));
    }

    @Test
    public void testIsNotArc() {
        assertFalse(graph.isArc(3, 0));
    }

    @Test
    public void testAddArc() {
        graph.addArc(3, 0);

        assertEquals(graph.getNbArcs(), 8);
        assertTrue(graph.isArc(3, 0));
    }

    @Test
    public void testRemoveArc() {
        graph.removeArc(1, 2);

        assertEquals(graph.getNbArcs(), 6);
        assertFalse(graph.isArc(1, 2));
    }

    @Test (expected = RuntimeException.class)
    public void testAddArcFail() {
        graph.addArc(2, 1);
    }

    @Test (expected = RuntimeException.class)
    public void testRemoveArcFail() {
        graph.removeArc(0, 2);
    }

    @Test
    public void testGetSuccessors() {
        assertTrue(Arrays.equals(graph.getSuccessors(2), new int[] {1, 3}));
    }

    @Test
    public void testGetPredecessors() {
        assertTrue(Arrays.equals(graph.getPrececessors(3), new int[] {0, 2}));
    }

    @Test
    public void testComputeInverse() {
        IDirectedGraph transposedGraph = graph.computeInverse();

        assertArrayEquals(transposedGraph.toAdjacencyMatrix(), VAL_MATRIX_TRANSPOSE);
        assertEquals(transposedGraph.getNbArcs(), graph.getNbArcs());
        assertEquals(transposedGraph.getNbNodes(), graph.getNbNodes());
    }
}
