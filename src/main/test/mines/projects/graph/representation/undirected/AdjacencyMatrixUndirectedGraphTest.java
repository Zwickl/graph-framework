package mines.projects.graph.representation.undirected;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;

/**
 * Test class for adjacency matrix based undirected graphs
 *
 * @author Paul Defois
 */
public class AdjacencyMatrixUndirectedGraphTest {
    private final int[][] VAL_MATRIX = new int[][]{
            new int[]{0, 1, 0, 1}, // 0
            new int[]{1, 0, 1, 1}, // 1
            new int[]{0, 1, 0, 0}, // 2
            new int[]{1, 1, 0, 0}  // 3
            //        0  1  2  3
    };
    private AdjacencyMatrixUndirectedGraph graph;

    @Before
    public void setup() {
        graph = new AdjacencyMatrixUndirectedGraph(VAL_MATRIX);
    }

    @Test
    public void testGetNbNodes() {
        assertEquals(graph.getNbNodes(), 4);
    }

    @Test
    public void testGetNbEdges() {
        assertEquals(graph.getNbEdges(), 4);
    }

    @Test
    public void testToAdjacencyMatrix() {
        assertEquals(graph.toAdjacencyMatrix(), VAL_MATRIX);
    }

    @Test
    public void testGetAdjacencyMatrix() {
        assertEquals(graph.getAdjacencyMatrix(), VAL_MATRIX);
    }

    @Test
    public void testIsEdge() {
        assertTrue(graph.isEdge(0, 1));
        assertTrue(graph.isEdge(1, 0));
    }

    @Test
    public void testIsNotEdge() {
        assertFalse(graph.isEdge(3, 2));
        assertFalse(graph.isEdge(2, 3));
    }

    @Test
    public void testAddEdge() {
        graph.addEdge(3, 2);

        assertEquals(graph.getNbEdges(), 5);
        assertTrue(graph.isEdge(2, 3));
        assertTrue(graph.isEdge(3, 2));
    }

    @Test
    public void testRemoveEdge() {
        graph.removeEdge(1, 2);

        assertEquals(graph.getNbEdges(), 3);
        assertFalse(graph.isEdge(2, 1));
        assertFalse(graph.isEdge(1, 2));
    }

    @Test (expected = RuntimeException.class)
    public void testAddEdgeFail() {
        graph.addEdge(0, 1);
    }

    @Test (expected = RuntimeException.class)
    public void testRemoveEdgeFail() {
        graph.removeEdge(3, 2);
    }

    @Test
    public void testGetNeighbors() {
        assertTrue(Arrays.equals(graph.getNeighbors(1), new int[] {0, 2, 3}));
    }
}
