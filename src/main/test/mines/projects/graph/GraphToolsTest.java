package mines.projects.graph;

import org.junit.Test;
import static org.junit.Assert.*;

public class GraphToolsTest {
    @Test
    public void computeOrderTest() {
        int[] tab = {10, 20, 8, 5, 13, 7, 9, 18, 17, 19};
        int[] result = {3, 5, 2, 6, 0, 4, 8, 7, 9, 1};

        assertArrayEquals(result, GraphTools.computeOrder(tab, true));
    }

    @Test
    public void computeOrderTest2() {
        int[] tab = {10, 20, 8, 5, 13, 7, 9, 18, 17, 19};
        int[] result = {1, 9, 7, 8, 4, 0, 6, 2, 5, 3};

        assertArrayEquals(result, GraphTools.computeOrder(tab, false));
    }

    @Test
    public void isSymetricTest() {
        int[][] matrix = GraphTools.generateGraphData(5, 5, 1, true);
        assertTrue(GraphTools.isSymetric(matrix));
    }

    @Test
    public void isNotSymetricTest() {
        int[][] matrix = GraphTools.generateGraphData(5, 5, 1, false);
        assertFalse(GraphTools.isSymetric(matrix));
    }
}
