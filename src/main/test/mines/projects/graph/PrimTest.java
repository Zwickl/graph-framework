package mines.projects.graph;

import mines.projects.graph.display.GraphDisplayOptions;
import mines.projects.graph.display.GraphToDot;
import mines.projects.graph.display.WindowsLauncher;
import mines.projects.graph.representation.undirected.AdjacencyMatrixUndirectedGraph;

public class PrimTest {
    private final static boolean DISPLAY = false;

    public static void main(String args[]) {
        int matrix[][] = new int[][]{
                {0, 1, 0, 1, 0},
                {1, 0, 1, 1, 0},
                {0, 1, 0, 1, 1},
                {1, 1, 1, 0, 1},
                {0, 0, 1, 1, 0}
        };

        AdjacencyMatrixUndirectedGraph graph = new AdjacencyMatrixUndirectedGraph(matrix);

        // Modification des poids
        graph.setWeight(0, 1, 1);
        graph.setWeight(0, 3, 5);
        graph.setWeight(1, 2, 3);
        graph.setWeight(2, 3, 0);
        graph.setWeight(2, 4, 2);
        graph.setWeight(3, 1, 1);
        graph.setWeight(3, 4, 1);

        graph.setWeight(1, 0, 1);
        graph.setWeight(3, 0, 5);
        graph.setWeight(2, 1, 3);
        graph.setWeight(3, 2, 0);
        graph.setWeight(4, 2, 2);
        graph.setWeight(1, 3, 1);
        graph.setWeight(4, 3, 1);

        Object[] result = GraphTools.prim(graph);
        int[] weights = (int[]) result[0];
        int[] predecessors = (int[]) result[1];
        AdjacencyMatrixUndirectedGraph MST = (AdjacencyMatrixUndirectedGraph) result[2];

        System.out.println("Arcs de l'arbre de poids recouvrant minimal  :");
        for (int i = 1; i < graph.getNbNodes(); i++) {
            System.out.println(predecessors[i] + " - " + i + "    " + graph.getWeight(i, predecessors[i]));
        }

        if (DISPLAY) {
            GraphDisplayOptions.getGraphDisplayOptions().displayArcCosts = true;
            GraphDisplayOptions.getGraphDisplayOptions().launcher = new WindowsLauncher("C:\\Program Files (x86)\\Graphviz2.38\\bin");
            GraphToDot.displayImage(GraphToDot.graphToDot(false, graph), "GraphePondere");
            GraphToDot.displayImage(GraphToDot.graphToDot(false, MST), "MST");

            /*
                    AFFICHAGE DES GRAPHES NON ORIENTES INCORRECT
            */
        }
    }
}
