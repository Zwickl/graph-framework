package mines.projects.graph;

import mines.projects.graph.display.GraphDisplayOptions;
import mines.projects.graph.display.GraphToDot;
import mines.projects.graph.display.WindowsLauncher;
import mines.projects.graph.explorer.undirected.UndirectedGraphWidthExplorer;
import mines.projects.graph.representation.GraphRepresentationFactory;
import mines.projects.graph.representation.Representation;
import mines.projects.graph.representation.directed.AdjacencyMatrixDirectedGraph;
import mines.projects.graph.representation.directed.IDirectedGraph;
import mines.projects.graph.representation.tree.BinaryHeap;
import mines.projects.graph.representation.tree.ITree;
import mines.projects.graph.representation.undirected.AdjacencyListUndirectedGraph;
import mines.projects.graph.representation.undirected.IUndirectedGraph;

import java.util.Map;

public class DisplayTest {
    public static void main(String args[]) {
        IGraph graph = GraphRepresentationFactory.createGraph(
                Representation.ADJACENCY_MATRIX,
                GraphTools.generateGraphData(10, 20, 5, false)
        );

//        int[][] matrix = {
//                {0, 1, 0, 5, 0},
//                {0, 0, 3, 0, 0},
//                {0, 0, 0, 0, 0},
//                {0, 0, 0, 0, 0},
//                {0, 0, 0, 0, 0},
//        };
//
//        IGraph graph = new AdjacencyMatrixDirectedGraph(matrix);

        GraphDisplayOptions.getGraphDisplayOptions().launcher = new WindowsLauncher("C:\\Program Files (x86)\\Graphviz2.38\\bin");


        IDirectedGraph composantesConnexes = (IDirectedGraph) GraphTools.calculComposantesFortementConnexes((IDirectedGraph) graph)[0];

        //      GraphDisplayOptions.getGraphDisplayOptions().displayArcCosts = true;


        GraphToDot.displayImage(GraphToDot.graphToDot(false, graph), "Graph");
        GraphToDot.displayImage(GraphToDot.graphToDot(false, composantesConnexes), "DeepExploreInverse");
//
//        Node[] val = {
//                new Node(0, 0),
//                new Node(1, 5),
//                new Node(2, 10),
//                new Node(3, 25),
//                new Node(4, 78),
//                new Node(5, 26),
//                new Node(6, 45),
//                null, null,
//                new Node(9, 105),
//                null, null,
//                new Node(12, 35),
//                null, null
//        };
//
//        GraphDisplayOptions.getGraphDisplayOptions().useValue = true;
//
//
//        ITree binaryTree = new BinaryHeap(val.length);
//        binaryTree.setNodes(val);

//        GraphToDot.displayImage(GraphToDot.graphToDot(false, binaryTree), "Arbre");
    }
}
