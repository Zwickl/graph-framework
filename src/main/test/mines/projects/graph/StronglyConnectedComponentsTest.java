package mines.projects.graph;

import mines.projects.graph.display.GraphDisplayOptions;
import mines.projects.graph.display.GraphToDot;
import mines.projects.graph.display.WindowsLauncher;
import mines.projects.graph.representation.GraphRepresentationFactory;
import mines.projects.graph.representation.Representation;
import mines.projects.graph.representation.directed.IDirectedGraph;

import java.util.List;

public class StronglyConnectedComponentsTest {
    private final static boolean DISPLAY = false;

    public static void main(String args[]) {
        IGraph graph = GraphRepresentationFactory.createGraph(
                Representation.ADJACENCY_MATRIX,
                GraphTools.generateGraphData(10, 20, 5, false)
        );

        Object[] result = GraphTools.calculComposantesFortementConnexes((IDirectedGraph) graph);
        IDirectedGraph graphComposantesConnexes = (IDirectedGraph) result[0];
        List<Integer[]> composantesConnexes = (List<Integer[]>) result[1];

        if (DISPLAY) {
            GraphDisplayOptions.getGraphDisplayOptions().launcher = new WindowsLauncher("C:\\Program Files (x86)\\Graphviz2.38\\bin");
            GraphToDot.displayImage(GraphToDot.graphToDot(false, graph), "Graph");
            GraphToDot.displayImage(GraphToDot.graphToDot(false, graphComposantesConnexes), "DeepExploreInverse");
        }

        System.out.println("Composantes fortements connexes :");
        for (Integer[] compoanteConnexe : composantesConnexes) {
            System.out.println(GraphTools.arrayToString(compoanteConnexe));
        }
    }
}
