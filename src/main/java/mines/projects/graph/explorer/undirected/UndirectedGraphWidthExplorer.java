package mines.projects.graph.explorer.undirected;

import mines.projects.graph.explorer.AbstractGraphExplorer;
import mines.projects.graph.representation.undirected.IUndirectedGraph;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/**
 * Imperative implementation of the undirected graph width exploration
 *
 * @author Paul Defois
 */
public class UndirectedGraphWidthExplorer extends AbstractGraphExplorer<IUndirectedGraph> {
    private boolean[] mark;

    public UndirectedGraphWidthExplorer(IUndirectedGraph graph) {
        super(graph);

        this.mark = new boolean[graph.getNbNodes()];
        Arrays.fill(mark, false);
    }

    @Override
    public void exploreNode(int node, Set<Integer> explored) {
        Queue<Integer> toVisit = new LinkedList<>();

        this.mark[node] = true;
        toVisit.add(node);

        while(!toVisit.isEmpty()) {
            int v = toVisit.poll();
            begin[v] = ++time;

            for(int w : this.graph.getNeighbors(v)) {
                if(!this.mark[w]) {
                    this.mark[w] = true;
                    this.inductedTree.addArc(v, w);
                    toVisit.add(w);
                }
            }

            end[v] = ++time;
        }
    }

    @Override
    public void exploreGraph() {
        for(int i = 0; i < graph.getNbNodes(); i++) {
            if(!this.mark[i]) {
                exploreNode(i, null);
            }
        }
    }
}
