package mines.projects.graph.explorer.undirected;

import mines.projects.graph.explorer.AbstractGraphExplorer;
import mines.projects.graph.representation.undirected.IUndirectedGraph;

import java.util.HashSet;
import java.util.Set;

/**
 * Recursive implementation of the directed graph deep exploration
 *
 * @author Paul Defois
 */
public class UndirectedGraphDeepExplorer extends AbstractGraphExplorer<IUndirectedGraph> {
    public UndirectedGraphDeepExplorer(IUndirectedGraph graph) {
        super(graph);
    }

    @Override
    public void exploreNode(int node, Set<Integer> explored) {
        explored.add(node);
        begin[node] = ++time;
        for(int t : graph.getNeighbors(node)) {
            if (!explored.contains(t)) {
                exploreNode(t, explored);
                this.inductedTree.addArc(node, t);
            }
        }
        end[node] = ++time;
    }

    @Override
    public void exploreGraph() {
        Set<Integer> explored = new HashSet<>();
        for(int i = 0; i < graph.getNbNodes(); i++) {
            if(!explored.contains(i)) {
                exploreNode(i, explored);
            }
        }
    }
}
