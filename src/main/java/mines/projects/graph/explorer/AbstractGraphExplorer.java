package mines.projects.graph.explorer;

import mines.projects.graph.GraphTools;
import mines.projects.graph.IGraph;
import mines.projects.graph.representation.directed.AdjacencyMatrixDirectedGraph;
import mines.projects.graph.representation.directed.IDirectedGraph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractGraphExplorer<T extends IGraph> implements IGraphExplorer {
    protected T graph;
    protected IDirectedGraph inductedTree;
    protected int[] begin;
    protected int[] end;
    protected int time;
    protected int[] order;
    protected List<Integer[]> SCC;

    public AbstractGraphExplorer(T graph) {
        this.graph = graph;
        this.begin = new int[graph.getNbNodes()];
        this.end = new int[graph.getNbNodes()];
        this.time = 0;
        this.inductedTree = new AdjacencyMatrixDirectedGraph(GraphTools.getBaseZeroMatrix(graph.getNbNodes()));

        Arrays.fill(begin, 0);
        Arrays.fill(end, 0);

        this.order = new int[graph.getNbNodes()];
        for(int i = 0; i < order.length; i++) {
            this.order [i] = i;
        }

        this.SCC = new ArrayList<>();
    }

    @Override
    public void setOrder(int[] order) {
        this.order = order;
    }

    @Override
    public IDirectedGraph getInductedGraph() {
        return this.inductedTree;
    }

    @Override
    public int[] getBeginTime() {
        return this.begin;
    }

    @Override
    public int[] getEndTime() {
        return this.end;
    }

    @Override
    public List<Integer[]> getSCC() { return this.SCC; }
}
