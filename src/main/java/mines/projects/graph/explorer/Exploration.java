package mines.projects.graph.explorer;

/**
 * List types of exploration
 *
 * @author Paul Defois
 */
public enum Exploration {
    WIDTH, DEEP
}
