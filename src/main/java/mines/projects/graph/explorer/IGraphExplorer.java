package mines.projects.graph.explorer;

import mines.projects.graph.representation.directed.IDirectedGraph;

import java.util.List;
import java.util.Set;

/**
 * Interface to generalize the exploration of graphs
 *
 * @author Paul Defois
 */
public interface IGraphExplorer {
    /**
     * Explore a graph starting with a node
     *
     * @param node the node of the graph where to start exploration
     * @param explored the set of node already explored
     */
    void exploreNode(int node, Set<Integer> explored);

    /**
     * Explore the graph
     */
    void exploreGraph();

    /**
     * Get the inducted graph built by the exploration of the graph
     *
     * @return the inducted graph
     */
    IDirectedGraph getInductedGraph();

    int[] getBeginTime();

    int[] getEndTime();

    void setOrder(int[] order);

    List<Integer[]> getSCC();
}
