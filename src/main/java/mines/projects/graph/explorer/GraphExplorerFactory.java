package mines.projects.graph.explorer;

import mines.projects.graph.IGraph;
import mines.projects.graph.explorer.directed.DirectedGraphDeepExplorer;
import mines.projects.graph.explorer.directed.DirectedGraphWidthExplorer;
import mines.projects.graph.explorer.undirected.UndirectedGraphDeepExplorer;
import mines.projects.graph.explorer.undirected.UndirectedGraphWidthExplorer;
import mines.projects.graph.representation.directed.IDirectedGraph;
import mines.projects.graph.representation.undirected.IUndirectedGraph;

/**
 * Factory for graph explorer
 *
 * @author Paul Defois
 */
public class GraphExplorerFactory {
    private GraphExplorerFactory() {}

    public static IGraphExplorer createExplorer(Exploration exploration, IGraph graph) {
        switch (exploration) {
            case DEEP:
                if (graph instanceof IDirectedGraph) return new DirectedGraphDeepExplorer((IDirectedGraph) graph);
                if (graph instanceof IUndirectedGraph) return new UndirectedGraphDeepExplorer((IUndirectedGraph) graph);
                break;
            case WIDTH:
                if (graph instanceof IDirectedGraph) return new DirectedGraphWidthExplorer((IDirectedGraph) graph);
                if (graph instanceof IUndirectedGraph) return new UndirectedGraphWidthExplorer((IUndirectedGraph) graph);
                break;
        }

        throw new RuntimeException("Error when creating the graph explorer");
    }
}
