package mines.projects.graph.explorer.directed;

import mines.projects.graph.explorer.AbstractGraphExplorer;
import mines.projects.graph.representation.directed.IDirectedGraph;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/**
 * Imperative implementation of the directed graph width exploration
 *
 * @author Paul Defois
 */
public class DirectedGraphWidthExplorer extends AbstractGraphExplorer<IDirectedGraph> {
    private boolean[] mark;

    public DirectedGraphWidthExplorer(IDirectedGraph graph) {
        super(graph);

        this.mark = new boolean[graph.getNbNodes()];
        Arrays.fill(mark, false);
    }

    @Override
    public void exploreNode(int node, Set<Integer> explored) {
        Queue<Integer> toVisit = new LinkedList<>();

        this.mark[node] = true;
        toVisit.add(node);

        while(!toVisit.isEmpty()) {
            int v = toVisit.poll();
            this.begin[v] = ++time;

            for(int w : this.graph.getSuccessors(v)) {
                if(!this.mark[w]) {
                    this.mark[w] = true;
                    this.inductedTree.addArc(v, w);
                    toVisit.add(w);
                }
            }

            this.end[v] = ++time;
        }
    }

    @Override
    public void exploreGraph() {
        for(int i = 0; i < graph.getNbNodes(); i++) {
            if(!this.mark[i]) {
                exploreNode(i, null);
            }
        }
    }
}
