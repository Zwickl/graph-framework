package mines.projects.graph.explorer.directed;

import mines.projects.graph.explorer.AbstractGraphExplorer;
import mines.projects.graph.representation.directed.IDirectedGraph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Recursive implementation of the directed graph deep exploration
 *
 * @author Paul Defois
 */
public class DirectedGraphDeepExplorer extends AbstractGraphExplorer<IDirectedGraph> {
    private List<Integer> aSCC;

    public DirectedGraphDeepExplorer(IDirectedGraph graph) {
        super(graph);

        aSCC = new ArrayList<>();
    }

    @Override
    public void exploreNode(int node, Set<Integer> explored) {
        explored.add(node);
        aSCC.add(node);
        begin[node] = ++time;
        for (int t : graph.getSuccessors(node)) {
            if (!explored.contains(t)) {
                exploreNode(t, explored);
                this.inductedTree.addArc(node, t);
            }
        }
        end[node] = ++time;
    }

    @Override
    public void exploreGraph() {
        Set<Integer> explored = new HashSet<>();
        for (int i = 0; i < order.length; i++) {
            if (!explored.contains(this.order[i])) {
                exploreNode(this.order[i], explored);

                this.SCC.add(aSCC.toArray(new Integer[aSCC.size()]));
            }
            aSCC = new ArrayList<>();
        }
    }
}
