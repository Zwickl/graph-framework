package mines.projects.graph.representation;

import mines.projects.graph.GraphTools;
import mines.projects.graph.IGraph;
import mines.projects.graph.representation.directed.AdjacencyListDirectedGraph;
import mines.projects.graph.representation.directed.AdjacencyMatrixDirectedGraph;
import mines.projects.graph.representation.undirected.AdjacencyListUndirectedGraph;
import mines.projects.graph.representation.undirected.AdjacencyMatrixUndirectedGraph;

/**
 * Factory for graph representations
 *
 * @author Paul Defois
 */
public class GraphRepresentationFactory {
    private GraphRepresentationFactory() {}

    public static IGraph createGraph(Representation representation, int[][] matrix) {
        switch (representation) {
            case ADJACENCY_LIST:
                if (GraphTools.isSymetric(matrix)) return new AdjacencyListUndirectedGraph(matrix);
                else return new AdjacencyListDirectedGraph(matrix);
            case ADJACENCY_MATRIX:
                if (GraphTools.isSymetric(matrix)) return new AdjacencyMatrixUndirectedGraph(matrix);
                else return new AdjacencyMatrixDirectedGraph(matrix);
        }

        throw new RuntimeException("Error when creating graph representation");
    }
}
