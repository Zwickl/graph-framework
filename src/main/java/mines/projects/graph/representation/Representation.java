package mines.projects.graph.representation;

/**
 * List types of graph representations
 *
 * @author Paul Defois
 */
public enum  Representation {
    ADJACENCY_LIST, ADJACENCY_MATRIX
}
