package mines.projects.graph.representation.tree;

import mines.projects.graph.IGraph;
import mines.projects.graph.Node;

/**
 * Create a graph representation tree based
 *
 * @author Paul Defois
 */
public interface ITree extends IGraph {

}
