package mines.projects.graph.representation.undirected;

import mines.projects.graph.Node;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Create an undirected adjacency list based graph representation
 *
 * @author Paul Defois
 */
public class AdjacencyListUndirectedGraph implements IUndirectedGraph {
    private Map<Integer, List<Node>> adjacencyList;
    private int nbNode;
    private int nbEdge;

    public AdjacencyListUndirectedGraph(int[][] matrix) {
        this.adjacencyList = new HashMap<>();
        this.nbNode = 0;
        this.nbEdge = 0;

        for (int line = 0; line < matrix.length; line++) {
            List<Node> neighbors = new ArrayList<>();
            for (int cell = 0; cell < matrix[line].length; cell++) {
                if(matrix[line][cell] == 1) {
                    neighbors.add(new Node(cell));
                    this.nbEdge++;
                }
            }
            this.adjacencyList.put(line, neighbors);
            this.nbNode++;
        }

        this.nbEdge /= 2;
    }

    public AdjacencyListUndirectedGraph(IUndirectedGraph undirectedGraph) {
        this(undirectedGraph.toAdjacencyMatrix());
    }

    @Override
    public Map<Integer, List<Node>> getAdjacencyList() {
        return adjacencyList;
    }

    @Override
    public int getNbNodes() {
        return this.nbNode;
    }

    @Override
    public int getNbEdges() {
        return this.nbEdge;
    }

    @Override
    public int[][] toAdjacencyMatrix() {
        int matrix[][] = new int[adjacencyList.size()][adjacencyList.size()];

        for(int node : adjacencyList.keySet()) {
            List<Node> neighbors = adjacencyList.get(node);

            for(int i = 0; i < adjacencyList.size(); i++) {
                matrix[node][i] = neighbors.contains(new Node(i)) ? 1 : 0;
            }
        }

        return matrix;
    }

    @Override
    public boolean isEdge(int x, int y) {
        return adjacencyList.get(x).contains(new Node(y)) && adjacencyList.get(y).contains(new Node(x));
    }

    @Override
    public void removeEdge(int x, int y) {
        if (isEdge(x, y)) {
            this.adjacencyList.get(x).remove(new Node(y));
            this.adjacencyList.get(y).remove(new Node(x));
            this.nbEdge--;
        } else {
            throw new RuntimeException("This edge don't exists, can't remove");
        }
    }

    @Override
    public void addEdge(int x, int y) {
        if (!isEdge(x, y)) {
            this.adjacencyList.get(x).add(new Node(y));
            this.adjacencyList.get(y).add(new Node(x));
            this.nbEdge++;
        } else {
            throw new RuntimeException("This edge already exists, can't add");
        }
    }

    @Override
    public int[] getNeighbors(int x) {
        int[] res = new int[adjacencyList.get(x).size()];
        for(int n = 0; n < adjacencyList.get(x).size(); n++) res[n] = adjacencyList.get(x).get(n).getName();

        return res;
    }

    @Override
    public String toString() {
        StringBuffer strb = new StringBuffer();

        for(int n : adjacencyList.keySet()) {
            strb.append(n).append(" -> ");

            for(Node v : adjacencyList.get(n)) {
                strb.append(v.getName()).append(" ");
            }
            strb.append("\n");
        }

        return strb.toString();
    }
}
