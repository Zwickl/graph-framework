package mines.projects.graph.representation.undirected;

import mines.projects.graph.representation.IGraphWeighted;

import java.util.ArrayList;
import java.util.List;

/**
 * Create an undirected adjacency matrix based graph representation
 *
 * @author Paul Defois
 */
public class AdjacencyMatrixUndirectedGraph implements IUndirectedGraph, IGraphWeighted {
    private int[][] adjacencyMatrix;
    private int[][] weightsMatrix;
    private int nbNode;
    private int nbEdge;

    public AdjacencyMatrixUndirectedGraph(int[][] matrix) {
        this.adjacencyMatrix = matrix;
        this.nbNode = matrix.length;
        this.weightsMatrix = new int[matrix.length][matrix.length];

        for(int line = 0; line < matrix.length; line++) {
            for(int cell = 0; cell < matrix[line].length; cell++) {
                if(matrix[line][cell] == 1) nbEdge++;
                this.weightsMatrix[line][cell] = Integer.MAX_VALUE;
            }
        }

        this.nbEdge /= 2;
    }

    public AdjacencyMatrixUndirectedGraph(IUndirectedGraph undirectedGraph) {
        this(undirectedGraph.toAdjacencyMatrix());
    }

    @Override
    public void setWeights(int[][] weights) {
        if(weights.length == getNbNodes()) {
            this.weightsMatrix = weights;
        } else {
            throw new RuntimeException("Error, the matrix must have the same size");
        }
    }

    @Override
    public int[][] getWeights() {
        return this.weightsMatrix;
    }

    @Override
    public int getWeight(int x, int y) {
        if(x >= getNbNodes() || y >= getNbNodes()) throw new RuntimeException("Error, x or y is out bounded");

        return this.weightsMatrix[x][y];
    }

    @Override
    public void setWeight(int x, int y, int weight) {
        if(x >= getNbNodes() || y >= getNbNodes()) throw new RuntimeException("Error, x or y is out bounded");

        this.weightsMatrix[x][y] = weight;
    }

    @Override
    public boolean isWeighted(int x, int y) {
        if(x >= getNbNodes() || y >= getNbNodes()) throw new RuntimeException("Error, x or y is out bounded");

        return this.adjacencyMatrix[x][y] >= Integer.MAX_VALUE;
    }

    @Override
    public int getNbNodes() {
        return this.nbNode;
    }

    @Override
    public int getNbEdges() {
        return this.nbEdge;
    }

    @Override
    public boolean isEdge(int x, int y) {
        return adjacencyMatrix[x][y] == 1 || adjacencyMatrix[y][x] == 1;
    }

    @Override
    public int[][] toAdjacencyMatrix() {
        return this.adjacencyMatrix;
    }

    @Override
    public void removeEdge(int x, int y) {
        if (isEdge(x, y)) {
            this.adjacencyMatrix[x][y] = 0;
            this.adjacencyMatrix[y][x] = 0;
            this.nbEdge--;
        } else {
            throw new RuntimeException("This edge don't exists, can't remove");
        }
    }

    @Override
    public void addEdge(int x, int y) {
        if (!isEdge(x, y)) {
            this.adjacencyMatrix[x][y] = 1;
            this.adjacencyMatrix[y][x] = 1;
            this.nbEdge++;
        } else {
            throw new RuntimeException("This edge already exists, can't add");
        }
    }

    @Override
    public int[] getNeighbors(int x) {
        List<Integer> neighbors = new ArrayList<>();

        for(int cell = 0; cell < adjacencyMatrix[x].length; cell++) {
            if(adjacencyMatrix[x][cell] == 1) neighbors.add(cell);
        }

        int[] res = new int[neighbors.size()];

        for(int i = 0; i < res.length; i++) {
            res[i] = neighbors.get(i);
        }

        return res;
    }

    @Override
    public int[][] getAdjacencyMatrix() {
        return this.adjacencyMatrix;
    }

    @Override
    public String toString() {
        StringBuffer strb = new StringBuffer();

        for(int line = 0; line < adjacencyMatrix.length; line++) {
            for(int cell = 0; cell < adjacencyMatrix[line].length; cell++) {
                strb.append(adjacencyMatrix[line][cell]).append(" ");
            }
            strb.append("\n");
        }

        return strb.toString();
    }
}
