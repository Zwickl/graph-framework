package mines.projects.graph.representation.undirected;

import mines.projects.graph.IGraph;

/**
 * Created by paul on 13/01/17.
 */
public interface IUndirectedGraph extends IGraph {
    int getNbEdges();
    boolean isEdge(int x, int y);
    void removeEdge(int x, int y);
    void addEdge(int x, int y);
    int[] getNeighbors(int x);
}
