package mines.projects.graph.representation.directed;

import mines.projects.graph.IGraph;

public interface IDirectedGraph extends IGraph {
    /**
     * Returns the number of arcs in the mines.projects.graph
     */
    int getNbArcs();

    /**
     * Returns true if arc (from,to) figures in the mines.projects.graph
     *
     * @param from the first node
     * @param to the second node
     */
    boolean isArc(int from, int to);

    void removeArc(int from, int to);

    void addArc(int from, int to);

    int[] getSuccessors(int x);

    int[] getPrececessors(int x);

    IDirectedGraph computeInverse();
}
