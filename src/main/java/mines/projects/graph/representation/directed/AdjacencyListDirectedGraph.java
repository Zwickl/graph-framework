package mines.projects.graph.representation.directed;

import mines.projects.graph.Node;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Create a directed adjacency list based graph representation
 *
 * @author Paul Defois
 */
public class AdjacencyListDirectedGraph implements IDirectedGraph {
    private Map<Integer, List<Node>> adjacencyList;
    private int nbNodes;
    private int nbArcs;

    public AdjacencyListDirectedGraph(int[][] matrix) {
        this.adjacencyList = new HashMap<>();
        this.nbNodes = 0;
        this.nbArcs = 0;

        for (int line = 0; line < matrix.length; line++) {
            List<Node> neighbors = new ArrayList<>();
            for (int cell = 0; cell < matrix[line].length; cell++) {
                if(matrix[line][cell] == 1) {
                    neighbors.add(new Node(cell));
                    this.nbArcs++;
                }
            }
            this.adjacencyList.put(line, neighbors);
            this.nbNodes++;
        }
    }

    public AdjacencyListDirectedGraph(IDirectedGraph directedGraph) {
        this(directedGraph.toAdjacencyMatrix());
    }

    @Override
    public int getNbNodes() {
        return this.nbNodes;
    }

    @Override
    public Map<Integer, List<Node>> getAdjacencyList() {
        return this.adjacencyList;
    }

    @Override
    public int getNbArcs() {
        return this.nbArcs;
    }

    @Override
    public int[][] toAdjacencyMatrix() {
        int matrix[][] = new int[adjacencyList.size()][adjacencyList.size()];

        for(int node : adjacencyList.keySet()) {
            List<Node> neighbors = adjacencyList.get(node);

            for(int i = 0; i < adjacencyList.size(); i++) {
                matrix[node][i] = neighbors.contains(new Node(i)) ? 1 : 0;
            }
        }

        return matrix;
    }

    @Override
    public boolean isArc(int from, int to) {
        return adjacencyList.get(from).contains(new Node(to));
    }

    @Override
    public void removeArc(int from, int to) {
        if (isArc(from, to)) {
            this.adjacencyList.get(from).remove(new Node(to));
            this.nbArcs--;
        } else {
            throw new RuntimeException("This arc don't exists, can't remove");
        }
    }

    @Override
    public void addArc(int from, int to) {
        if (!isArc(from, to)) {
            this.adjacencyList.get(from).add(new Node(to));
            this.nbArcs++;
        } else {
            throw new RuntimeException("This arc already exists, can't add");
        }
    }

    @Override
    public int[] getSuccessors(int x) {
        int[] res = new int[adjacencyList.get(x).size()];
        for(int n = 0; n < adjacencyList.get(x).size(); n++) res[n] = adjacencyList.get(x).get(n).getName();

        return res;
    }

    @Override
    public int[] getPrececessors(int x) {
        List<Integer> pred = new ArrayList<>();

        for(int node : adjacencyList.keySet()) {
            if(adjacencyList.get(node).contains(new Node(x))) pred.add(node);
        }

        int[] res = new int[pred.size()];
        for(int n = 0; n < pred.size(); n++) res[n] = pred.get(n);

        return res;
    }

    @Override
    public IDirectedGraph computeInverse() {
        int[][] matrix = new int[getNbNodes()][getNbNodes()];

        for(int[] line : matrix) Arrays.fill(line, 0);

        for(int node : adjacencyList.keySet()) {
            for(int pred : getPrececessors(node)) {
                matrix[node][pred] = 1;
            }
        }

        return new AdjacencyListDirectedGraph(matrix);
    }

    @Override
    public String toString() {
        StringBuffer strb = new StringBuffer();

        for(int n : adjacencyList.keySet()) {
            strb.append(n).append(" -> ");

            for(Node v : adjacencyList.get(n)) {
                strb.append(v.getName()).append(" ");
            }
            strb.append("\n");
        }

        return strb.toString();
    }
}
