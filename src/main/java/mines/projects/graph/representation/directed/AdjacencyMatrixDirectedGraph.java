package mines.projects.graph.representation.directed;

import mines.projects.graph.Node;
import mines.projects.graph.representation.IGraphWeighted;

import java.util.Arrays;

/**
 * Create a directed adjacency matrix based graph representation
 *
 * @author Paul Defois
 */
public class AdjacencyMatrixDirectedGraph implements IDirectedGraph, IGraphWeighted {
    private int[][] adjacencyMatrix;
    private int[][] weightsMatrix;
    private int nbNodes;
    private int nbArcs;
    private Node[] nodes;

    public AdjacencyMatrixDirectedGraph(int[][] matrix) {
        this.adjacencyMatrix = matrix;
        this.nbNodes = matrix.length;
        this.nbArcs = 0;
        this.nodes = new Node[matrix.length];
        this.weightsMatrix = new int[matrix.length][matrix.length];

        for(int line = 0; line < matrix.length; line++) {
            this.nodes[line] = new Node(line);

            for(int cell = 0; cell < matrix[line].length; cell++) {
                if(matrix[line][cell] == 1) nbArcs++;
                this.weightsMatrix[line][cell] = Integer.MAX_VALUE;
            }
        }
    }

    @Override
    public void setWeights(int[][] weights) {
        if(weights.length == getNbNodes()) {
            this.weightsMatrix = weights;
        } else {
            throw new RuntimeException("Error, the matrix must have the same size");
        }
    }

    @Override
    public int[][] getWeights() {
        return this.weightsMatrix;
    }

    @Override
    public int getWeight(int x, int y) {
        if(x >= getNbNodes() || y >= getNbNodes()) throw new RuntimeException("Error, x or y is out bounded");

        return this.weightsMatrix[x][y];
    }

    @Override
    public void setWeight(int x, int y, int weight) {
        if(x >= getNbNodes() || y >= getNbNodes()) throw new RuntimeException("Error, x or y is out bounded");

        this.weightsMatrix[x][y] = weight;
    }

    @Override
    public boolean isWeighted(int x, int y) {
        if(x >= getNbNodes() || y >= getNbNodes()) throw new RuntimeException("Error, x or y is out bounded");

        return this.adjacencyMatrix[x][y] >= Integer.MAX_VALUE;
    }

    @Override
    public Node[] getNodes() {
        return this.nodes;
    }

    @Override
    public int getNbNodes() {
        return this.nbNodes;
    }

    @Override
    public int[][] toAdjacencyMatrix() {
        return this.adjacencyMatrix;
    }

    @Override
    public int getNbArcs() {
        return this.nbArcs;
    }

    @Override
    public int[][] getAdjacencyMatrix() {
        return this.adjacencyMatrix;
    }

    @Override
    public boolean isArc(int from, int to) {
        return adjacencyMatrix[from][to] == 1;
    }

    @Override
    public void removeArc(int from, int to) {
        if (isArc(from, to)) {
            this.adjacencyMatrix[from][to] = 0;
            this.nbArcs--;
        } else {
            throw new RuntimeException("This arc don't exists, can't remove");
        }
    }

    @Override
    public void addArc(int from, int to) {
        if (!isArc(from, to)) {
            this.adjacencyMatrix[from][to] = 1;
            this.nbArcs++;
        } else {
            throw new RuntimeException("This arc already exists, can't add");
        }
    }

    @Override
    public int[] getSuccessors(int x) {
        int[] successors = new int[getNbNodes()];

        int k = 0;
        for(int cell = 0; cell < adjacencyMatrix[x].length; cell++) {
            if(adjacencyMatrix[x][cell] == 1) {
                successors[k] = cell;

                k++;
            }
        }

        return Arrays.copyOf(successors, k);
    }

    @Override
    public int[] getPrececessors(int x) {
        int[] predecessors = new int[getNbNodes()];

        int k = 0;
        for(int line = 0; line < adjacencyMatrix.length; line++) {
            if (adjacencyMatrix[line][x] == 1) {
                predecessors[k] = line;

                k++;
            }
        }

        return Arrays.copyOf(predecessors, k);
    }

    @Override
    public IDirectedGraph computeInverse() {
        int[][] matrix = new int[getNbNodes()][getNbNodes()];

        for(int[] line : matrix) Arrays.fill(line, 0);

        for(int line = 0; line < adjacencyMatrix.length; line++) {
            for(int cell = 0; cell < adjacencyMatrix[line].length; cell++) {
                if(adjacencyMatrix[line][cell] == 1) matrix[cell][line] = 1;
            }
        }

        return new AdjacencyMatrixDirectedGraph(matrix);
    }

    @Override
    public String toString() {
        StringBuffer strb = new StringBuffer();

        for(int line = 0; line < adjacencyMatrix.length; line++) {
            for(int cell = 0; cell < adjacencyMatrix[line].length; cell++) {
                strb.append(adjacencyMatrix[line][cell]).append(" ");
            }
            strb.append("\n");
        }

        return strb.toString();
    }
}
