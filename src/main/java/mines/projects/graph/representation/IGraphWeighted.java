package mines.projects.graph.representation;

public interface IGraphWeighted {
    int[][] getWeights();
    void setWeights(int[][] weights);

    int getWeight(int x, int y);
    void setWeight(int x, int y, int weight);

    boolean isWeighted(int x, int y);
}
