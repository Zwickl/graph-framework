package mines.projects.graph;

/**
 * Represent a graph node
 *
 * @author Paul Defois
 */
public class Node {
    private int name;
    private String label;
    private int value;

    public Node(int name, String label, int value) {
        this.name = name;
        this.label = label;
        this.value = value;
    }

    public Node(int name, int value) {
        this(name, "", value);
    }

    public Node(int name) {
        this(name, 0);
    }

    public String getLabel() {
        return label.equals("") ? "" + name : label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Node node = (Node) o;

        return name == node.name;

    }

    @Override
    public int hashCode() {
        return name;
    }
}
