package mines.projects.graph.display;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CommandExe {
    public void executeCommand(String[] command) {
        try {
            Process p = Runtime.getRuntime().exec(command);
            p.waitFor();

            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader readerErr = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            String line = "";
            while ((line = reader.readLine()) != null) {
                System.out.println(line + "\n");
            }

            while ((line = readerErr.readLine()) != null) {
                System.out.println(line + "\n");
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
