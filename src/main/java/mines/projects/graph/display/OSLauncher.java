package mines.projects.graph.display;

public interface OSLauncher {
    void displayImage(String content, String name, CommandExe commandExe);
}
