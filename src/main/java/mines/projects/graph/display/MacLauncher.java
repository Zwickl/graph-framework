package mines.projects.graph.display;

public class MacLauncher implements OSLauncher {

    @Override
    public void displayImage(String content, String name, CommandExe commandExe) {
        commandExe.executeCommand(new String[]{
                "sh",
                "-c",
                "echo \"" + content + "\" | dot -Tpng > " + name + ".png"
        });

        commandExe.executeCommand(new String[]{
                "sh",
                "-c",
                "open " + name + ".png"
        });
    }
}