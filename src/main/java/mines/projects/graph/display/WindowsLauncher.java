package mines.projects.graph.display;

import java.io.File;

public class WindowsLauncher implements OSLauncher {
    private String installDotPath;

    public WindowsLauncher(String installDotPath) {
        this.installDotPath = installDotPath.endsWith(File.separator) ? installDotPath + "dot.exe" : installDotPath + File.separator + "dot.exe";
    }

    @Override
    public void displayImage(String content, String name, CommandExe commandExe) {
        String contentParsed = content.replace(">", "^>");

        commandExe.executeCommand(new String[] {
                "cmd.exe",
                "/C",
                "echo " + contentParsed + " > " + name + ".dot"
        });

        commandExe.executeCommand(new String[] {
                "cmd.exe",
                "/C",
                "\""+installDotPath + "\" " + name + ".dot" + " -Tpng -o " + name + ".png"
        });

        commandExe.executeCommand(new String[] {
                "cmd.exe",
                "/C",
                "start " + name + ".png"
        });
    }
}
