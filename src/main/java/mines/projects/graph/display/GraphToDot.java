package mines.projects.graph.display;

import mines.projects.graph.IGraph;
import mines.projects.graph.representation.IGraphWeighted;
import mines.projects.graph.representation.directed.IDirectedGraph;
import mines.projects.graph.representation.tree.BinaryHeap;
import mines.projects.graph.representation.tree.BinaryTree;
import mines.projects.graph.representation.tree.ITree;

public class GraphToDot {
    private GraphToDot() {
    }

    private static GraphDisplayOptions options = GraphDisplayOptions.getGraphDisplayOptions();

    public static GraphDisplayOptions getOptions() {
        return options;
    }

    /**
     * Méthode méga crade à refractor .... qui génère un graphe au format dot
     *
     * @param returnLine indique si on fait des retours à la ligne dans le contenu généré
     * @param graph      le graphe à afficher
     * @return le contenu textuel au format dot du graphe à afficher
     */
    public static String graphToDot(boolean returnLine, IGraph graph) {
        boolean directed = graph instanceof IDirectedGraph || graph instanceof ITree;
        StringBuffer strb = new StringBuffer();
        String returnL = returnLine ? "\n" : "";
        int[][] matrix = graph.toAdjacencyMatrix();

        strb.append(directed ? "digraph" : "graph").append(" G {").append(returnL);
        for (int line = 0; line < matrix.length; line++) {
            for (int cell = directed ? 0 : line + 1; cell < matrix[line].length; cell++) {
                if (graph.getNodes() != null && graph.getNodes()[line] != null) strb.append(line);

                if (options.useLabel && graph.getNodes()[line] != null) {
                    strb.append(" [label=\"").append(graph.getNodes()[line].getLabel()).append("\"]");
                } else if (options.useValue && graph.getNodes()[line] != null) {
                    strb.append(" [label=\"").append(graph.getNodes()[line].getValue()).append("\"]");
                }

//                strb.append(";").append(returnL);
                if (matrix[line][cell] == 1) {
                    strb.append(line).append(directed ? " -> " : " -- ").append(cell);


                    if (options.displayArcCosts && graph instanceof IGraphWeighted) {
                        IGraphWeighted gw = (IGraphWeighted) graph;

                        strb.append(" [ label = \"" + gw.getWeight(line, cell) + "\"];");
                    } else {
                        strb.append(";").append(returnL);
                    }
                }
            }
        }

        strb.append("}");

        return strb.toString();
    }

    public static void displayImage(String content, String name) throws RuntimeException {
        if (getOptions().launcher == null)
            throw new RuntimeException("Error, a launcher have to be defined in the graph options");

        getOptions().launcher.displayImage(content, name, new CommandExe());
    }
}
