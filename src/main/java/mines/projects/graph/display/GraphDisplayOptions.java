package mines.projects.graph.display;

public class GraphDisplayOptions {

    private GraphDisplayOptions() { }

    private static GraphDisplayOptions options;
    public static GraphDisplayOptions getGraphDisplayOptions () {
        if (options == null) options = new GraphDisplayOptions();

        return options;
    }

    public boolean useLabel = false;
    public boolean useValue = false;
    public boolean displayArcCosts = false;
    public OSLauncher launcher = null;
}
