package mines.projects.graph;


import mines.projects.graph.explorer.Exploration;
import mines.projects.graph.explorer.GraphExplorerFactory;
import mines.projects.graph.explorer.IGraphExplorer;
import mines.projects.graph.representation.directed.AdjacencyMatrixDirectedGraph;
import mines.projects.graph.representation.directed.IDirectedGraph;
import mines.projects.graph.representation.undirected.AdjacencyMatrixUndirectedGraph;

import java.util.*;

/**
 * Tools used to manipulate graphs
 *
 * @author Paul Defois
 */
public class GraphTools {

    /**
     * Generate randomly an adgacency matrix
     *
     * @param n       the number of node
     * @param m       the number of arcs
     * @param coutMax not used
     * @param s       if the matrix have to be symetric
     * @return the generated matrix
     */
    public static int[][] generateGraphData(int n, int m, int coutMax, boolean s) {
        Random random = new Random();

        int matrix[][] = new int[n][n];

        // Initialisation à 0
        for (int[] line : matrix) {
            for (int cell : line) {
                cell = 0;
            }
        }

        // Génération du graph
        for (int i = 0; i < m; i++) {
            int x, y;

            do {
                x = random.nextInt(n);
                y = random.nextInt(n);
            } while (x == y || matrix[x][y] == 1);

            matrix[x][y] = 1;

            if (s) matrix[y][x] = 1;
        }

        return matrix;
    }

    /**
     * Tell us if a matrix is symetric
     *
     * @param matrix the matrix to be tested
     * @return if the matrix is symetric
     */
    public static boolean isSymetric(int[][] matrix) {
        boolean symetric = true;

        for (int line = 0; line < matrix.length; line++) {
            for (int cell = 0; cell < matrix[line].length; cell++) {
                if (matrix[line][cell] == 1 && matrix[cell][line] == 0) {
                    symetric = false;
                    break;
                }
            }
        }

        return symetric;
    }

    public static int[][] symetriseMatrix(int[][] matrix) {
        // TODO

        return new int[matrix.length][matrix.length];
    }

    /**
     * Used to generate a squared matrix filled with 0 (an empty graph)
     *
     * @param n the number of node (size of the matrix)
     * @return a zero matrix
     */
    public static int[][] getBaseZeroMatrix(int n) {
        int[][] matrix = new int[n][n];
        for (int[] line : matrix) Arrays.fill(line, 0);
        return matrix;
    }

    /**
     * Convert an int array to string
     *
     * @param array the array
     * @return the string array
     */
    public static <T> String arrayToString(T[] array) {
        StringBuffer strb = new StringBuffer();

        strb.append("[ ");
        for (T i : array) {
            strb.append(i.toString()).append(" ");
        }
        strb.append("]");

        return strb.toString();
    }

    public static String arrayToString(int[] array) {
        StringBuffer strb = new StringBuffer();

        strb.append("[ ");
        for (int i : array) {
            strb.append(i).append(" ");
        }
        strb.append("]");

        return strb.toString();
    }

    /**
     * Convert an int matrix to string
     *
     * @param matrix the matrix
     * @return the string matrix
     */
    public static <T> String matrixToString(T[][] matrix) {
        StringBuffer strb = new StringBuffer();

        for (T[] line : matrix) {
            strb.append(arrayToString(line)).append("\n");
        }

        return strb.toString();
    }

    public static String matrixToString(int[][] matrix) {
        StringBuffer strb = new StringBuffer();

        for (int[] line : matrix) {
            strb.append(arrayToString(line)).append("\n");
        }

        return strb.toString();
    }

    /**
     * Sort an array of node ascendent or descendent
     *
     * @param nodes     the array to be sorted
     * @param croissant ascendent or descendent
     * @return the array sorted
     */
    public static int[] computeOrder(int[] nodes, boolean croissant) {
        Map<Integer, Integer> nodeOrder = new HashMap<>();

        for (int i = 0; i < nodes.length; i++) {
            nodeOrder.put(nodes[i], i);
        }

        List<Integer> valOrdered = new ArrayList<>(nodeOrder.keySet());
        Collections.sort(valOrdered);

        if (!croissant) Collections.reverse(valOrdered);

        int[] order = new int[nodes.length];

        for (int i = 0; i < valOrdered.size(); i++) {
            order[i] = nodeOrder.get(valOrdered.get(i));
        }

        return order;
    }

    /**
     * Get the strongly connected components of a directed graph
     *
     * @param graph the graph to be explored
     * @return an inducted partial graph (tree) of the different strongly connected components
     */
    public static Object[] calculComposantesFortementConnexes(IDirectedGraph graph) {
        // Calcule du graph inverse
        IGraph graphInverse = graph.computeInverse();

        // Création de l'explorateur sur le graphe
        IGraphExplorer deepExplorer = GraphExplorerFactory.createExplorer(Exploration.DEEP, graph);
        IGraphExplorer deepExplorerInverse = GraphExplorerFactory.createExplorer(Exploration.DEEP, graphInverse);

        // Parcours du premier graphe en profondeur
        deepExplorer.exploreGraph();

        // Parcours du second graphe en profondeur ordonné par les date de fin décroissantes du parcours du premier graphe
        deepExplorerInverse.setOrder(GraphTools.computeOrder(deepExplorer.getEndTime(), false));
        deepExplorerInverse.exploreGraph();

        return new Object[]{deepExplorerInverse.getInductedGraph(), deepExplorerInverse.getSCC()};
    }

    /**
     * Apply the bellman algorithm on a graph
     *
     * @param graph       the graph
     * @param firstSommet the first node where the algorithm begins
     * @return the distance array and the predecessor array of the shorter path in the graph
     */
    public static Object[] bellman(AdjacencyMatrixDirectedGraph graph, int firstSommet) {
        int[] distance = new int[graph.getNbNodes()];
        int[] pred = new int[graph.getNbNodes()];

        Arrays.fill(distance, Integer.MAX_VALUE);
        Arrays.fill(pred, -1);

        distance[firstSommet] = 0;

        for (int k = 0; k < graph.getNbNodes() - 1; k++) {
            for (int x = 0; x < graph.getNbNodes(); x++) {
                for (int y : graph.getSuccessors(x)) {
                    if (distance[y] > distance[x] + graph.getWeight(x, y)) {
                        distance[y] = distance[x] + graph.getWeight(x, y);
                        pred[y] = x;
                    }
                }
            }
        }

        for (int x = 0; x < graph.getNbNodes(); x++) {
            for (int y : graph.getSuccessors(x)) {
                if (distance[x] + graph.getWeight(x, y) < distance[y]) {
                    throw new RuntimeException("Error, the graph contains a negative-weight cycle");
                }
            }
        }

        return new Object[]{distance, pred};
    }

    public static int minDistance(int distance[], boolean nodeVisited[]) {
        int min = Integer.MAX_VALUE;
        int min_index = -1;

        for (int n = 0; n < distance.length; n++) {
            if (!nodeVisited[n] && distance[n] <= min) {
                min = distance[n];
                min_index = n;
            }
        }

        return min_index;
    }

    public static Object[] dijkstra(AdjacencyMatrixDirectedGraph graph, int firstNode) {
        int[] distance = new int[graph.getNbNodes()];
        int[] pred = new int[graph.getNbNodes()];
        boolean[] visited = new boolean[graph.getNbNodes()];

        Arrays.fill(distance, Integer.MAX_VALUE);
        Arrays.fill(visited, false);
        Arrays.fill(pred, -1);

        distance[firstNode] = 0;

        for (int i = 0; i < graph.getNbNodes() - 1; i++) {
            int node = minDistance(distance, visited);

            visited[node] = true;

            for (int j = 0; j < graph.getNbNodes(); j++) {
                if (!visited[j] && graph.isArc(node, j) && distance[node] != Integer.MAX_VALUE && distance[node] + graph.getWeight(node, j) < distance[j]) {
                    distance[j] = distance[node] + graph.getWeight(node, j);
                    pred[j] = node;
                }
            }
        }

        return new Object[]{distance, pred};
    }

    public static Object[] floyd(AdjacencyMatrixDirectedGraph graph) {
        int distances[][] = graph.getWeights();
        int successors[][] = new int[graph.getNbNodes()][graph.getNbNodes()];

        for (int x = 0; x < graph.getNbNodes(); x++) {
            for (int y = 0; y < graph.getNbNodes(); y++) {
                successors[x][y] = y;
            }
        }

        for (int k = 0; k < graph.getNbNodes(); k++) {
            for (int i = 0; i < graph.getNbNodes(); i++) {
                for (int j = 0; j < graph.getNbNodes(); j++) {
                    if (distances[i][k] != Integer.MAX_VALUE && distances[k][j] != Integer.MAX_VALUE && distances[i][k] + distances[k][j] < distances[i][j]) {
                        distances[i][j] = distances[i][k] + distances[k][j];
                        successors[i][j] = successors[i][k];
                    }
                }
            }
        }

        return new Object[]{distances, successors};
    }

    public static int[] getPathFromFloydSuccessorsMatrix(int[][] successors, int x, int y) {
        if (successors[x][y] == 0) {
            return null;
        }

        int[] path = new int[successors.length];
        path[0] = x;

        int i = 1;
        while (x != y) {
            x = successors[x][y];
            path[i] = x;
            i++;
        }

        return Arrays.copyOf(path, i);
    }

    public static int minWeight(int weights[], boolean nodeVisited[]) {
        int min = Integer.MAX_VALUE;
        int min_index = -1;

        for (int n = 0; n < weights.length; n++) {
            if (!nodeVisited[n] && weights[n] < min) {
                min = weights[n];
                min_index = n;
            }
        }

        return min_index;
    }

    public static Object[] prim(AdjacencyMatrixUndirectedGraph graph) {
        int[] predecessor = new int[graph.getNbNodes()];
        int[] weights = new int[graph.getNbNodes()];
        boolean[] visited = new boolean[graph.getNbNodes()];

        AdjacencyMatrixUndirectedGraph MST = new AdjacencyMatrixUndirectedGraph(getBaseZeroMatrix(graph.getNbNodes()));

        Arrays.fill(weights, Integer.MAX_VALUE);
        Arrays.fill(visited, false);

        weights[0] = 0;
        predecessor[0] = -1;

        for (int k = 0; k < graph.getNbNodes() - 1; k++) {
            int node = minWeight(weights, visited);

            visited[node] = true;

            for (int j = 0; j < graph.getNbNodes(); j++) {
                if (graph.isEdge(node, j) && !visited[j] && graph.getWeight(node, j) < weights[j]) {
                    predecessor[j] = node;
                    weights[j] = graph.getWeight(node, j);
                    MST.addEdge(node, j);
                    MST.setWeight(node, j, weights[j]);
                }
            }
        }

        return new Object[] { weights, predecessor, MST };
    }
}