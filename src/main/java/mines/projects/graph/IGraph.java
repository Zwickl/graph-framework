package mines.projects.graph;

import java.util.List;
import java.util.Map;

public interface IGraph {
    int getNbNodes();
    int[][] toAdjacencyMatrix();

    default Node[] getNodes() {
        return null;
    }

    default Map<Integer, List<Node>> getAdjacencyList() {
        return null;
    }
    default int[][] getAdjacencyMatrix() {
        return null;
    }
}
